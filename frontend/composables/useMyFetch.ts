export const useMyFetch: typeof useFetch = (request, opts?) => {
	const config = useRuntimeConfig();
	const baseURL = process.server
		? config.public.ssrApiUrl
		: config.public.apiUrl;

	return useFetch(request, { baseURL, ...opts });
}
