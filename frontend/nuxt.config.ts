export default defineNuxtConfig({
	devtools: { enabled: false },

	typescript: {
		strict: true,
	},

  imports: {
    dirs: ['stores'],
  },

	modules: [
		'@nuxt/image-edge',
		'nuxt-icon',
		[
			'@pinia/nuxt',
			{
				autoImports: [
					'defineStore', 'acceptHMRUpdate',
					['defineStore', 'definePiniaStore'],
				],
			},
		],
	],

	css: [
		'@/assets/styles/main.scss',
	],

	runtimeConfig: {
		public: {
			apiUrl: process.env.API_URL || 'http://localhost:5000/api',
			ssrApiUrl: process.env.SSR_API_URL || 'http://localhost:5000/api',
		},
	},

	vite: {
		css: {
			preprocessorOptions: {
				scss: {
					additionalData: '@import "@/assets/styles/_variables.scss";',
				},
			},
		},
	},
})
