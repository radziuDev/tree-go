export const useLeafStore = defineStore({
	id: 'LeafStore',
	actions: {
		async addLeaf(id: string) {
			const treeStore = useTreeStore();
			treeStore.setFetchingTree(true);

			const { error } = await useMyFetch('/leaf', {
				method: 'POST',
				body: { rootedIn: id },
			});

			if (error?.value?.data) {
				treeStore.setFetchingTree(false);
				throw error.value?.data ?? 'Unexpected error!';
			}

			await treeStore.refetchCurrentTree();
		},

		async deleteLeaf(id: string) {
			const treeStore = useTreeStore();
			treeStore.setFetchingTree(true);

			const { error } = await useMyFetch(`/leaf/${id}`, {
				method: 'DELETE',
			});

			if (error?.value?.data) {
				treeStore.setFetchingTree(false);
				throw error.value?.data ?? 'Unexpected error!';
			}

			await treeStore.refetchCurrentTree();
		},
	},
});
