interface Tree {
	id: string,
	name: string,
}

interface CurrentTree {
	Id: number,
	Props: {
		id: string,
		name: string,
	},
	RootedIn: number,
	Type: string,
}

interface State {
	trees: Tree[],
	currentTree: CurrentTree | null,
	isFetchingTree: boolean,
};

export const useTreeStore = defineStore({
	id: 'TreeStore',
	state: (): State => {
		return {
			trees: [],
			currentTree: null,
			isFetchingTree: false,
		}
	},

	getters: {
		getTrees(state) {
			return state.trees;
		},

		getCurrentTree(state) {
			return state.currentTree;
		},

		getFetchingState(state) {
			return state.isFetchingTree;
		},
	},

	actions: {
		setFetchingTree(status: boolean) {
			this.isFetchingTree = status;
		},

		async fetchTrees() {
			const { data, error } = await useMyFetch('/tree');
			if (error?.value?.data) throw error.value?.data ?? 'Unexpected error!';
			this.trees = data as unknown as Tree[];
		},

		async addTree(name: string) {
			const { error } = await useMyFetch('/tree', { method: 'POST', body: { name } });
			this.fetchTrees();
			if (error?.value?.data) throw error.value?.data ?? 'Unexpected error!';
		},

		async editTree(id: string, name: string) {
			const { error } = await useMyFetch(`/tree/${id}/name`, { method: 'PATCH', body: { name } });
			this.fetchTrees();
			if (error?.value?.data) throw error.value?.data ?? 'Unexpected error!'
		},

		async deleteTree(name: string) {
			const { error } = await useMyFetch(`/tree/${name}`, { method: 'DELETE' });
			this.fetchTrees();
			if (error?.value?.data) throw error.value?.data ?? 'Unexpected error!';
		},

		async setCurrentTree(id: string) {
			this.isFetchingTree = true;
			const { error, data } = await useMyFetch(`/tree/${id}`, { method: 'GET' });
			if (error?.value?.data) {
				this.currentTree = null;
				this.isFetchingTree = false;
				throw error.value?.data ?? 'Unexpected error!';
			}
			this.currentTree = data.value as CurrentTree;
			this.isFetchingTree = false;
		},

		async refetchCurrentTree() {
			if (!this.currentTree) return this.isFetchingTree = false;
			const id = this.currentTree.Props?.id ?? (this.currentTree as any)?.Values[0]?.Props?.id;
			await this.setCurrentTree(id);
		},
	},
});
