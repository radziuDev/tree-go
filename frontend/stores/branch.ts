export const useBranchStore = defineStore({
	id: 'BranchStore',
	actions: {
		async addBranch(value: number | null, rootedIn: string, isMainBranch: boolean) {
			const treeStore = useTreeStore();
			treeStore.setFetchingTree(true);

			const { error } = await useMyFetch('/branch', {
				method: 'POST',
				body: { value: value ? +value : 0, rootedIn, isMainBranch },
			});

			if (error?.value?.data) {
				treeStore.setFetchingTree(false);
				throw error.value?.data ?? 'Unexpected error!';
			}

			await treeStore.refetchCurrentTree();
		},

		async editBranch(id: string, value: number | null, rootedIn: string, isMainBranch: boolean) {
			const treeStore = useTreeStore();
			treeStore.setFetchingTree(true);

			const { error } = await useMyFetch(`/branch/${id}`, {
				method: 'PUT',
				body: { value: value ? +value : 0, rootedIn, isMainBranch },
			});

			if (error?.value?.data) {
				treeStore.setFetchingTree(false);
				throw error.value?.data ?? 'Unexpected error!';
			}

			await treeStore.refetchCurrentTree();
		},

		async deleteBranch(id: string) {
			const treeStore = useTreeStore();
			treeStore.setFetchingTree(true);

			const { error } = await useMyFetch(`/branch/${id}`, {
				method: 'DELETE',
			});

			if (error?.value?.data) {
				treeStore.setFetchingTree(false);
				throw error.value?.data ?? 'Unexpected error!';
			}

			await treeStore.refetchCurrentTree();
		},
	},
});


