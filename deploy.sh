#!/bin/bash

if [ ! -f .env ]; then
    cp .env.example .env
fi

docker-compose up --build -d

echo -e "\n"
echo '/------UNIT-TESTS------/'
echo -e "\n"
docker-compose run backend sh -c 'go test ./...'
