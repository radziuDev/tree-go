package config

type ConfigStruct struct {
	Port          int
	Prefix        string
	Neo4jUrl      string
	Neo4jUser     string
	Neo4jPassword string
	RedisUrl			string
	RedisPassword string
}

var Config ConfigStruct

func LoadConfigs() {
	setConfig("Port", "PORT", true)
	setConfig("Prefix", "PREFIX", false)
	setConfig("Neo4jUrl", "NEO4J_URL", false)
	setConfig("Neo4jUser", "NEO4J_USER", false)
	setConfig("Neo4jPassword", "NEO4J_PASSWORD", false)
	setConfig("RedisUrl", "REDIS_URL", false)
	setConfig("RedisPassword", "REDIS_PASSWORD", false)
}
