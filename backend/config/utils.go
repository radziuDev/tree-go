package config

import (
	"log"
	"os"
	"reflect"
	"strconv"
)

func setConfig(key string, envKey string, parseInt bool) {
	value := os.Getenv(envKey)

	if value == "" {
		log.Printf("No configuration of the environment variable \"%v\".", envKey)
		os.Exit(1)
	}

	if parseInt {
		parsedValue, parseError := strconv.Atoi(value)
		if parseError != nil {
			log.Printf("Wrong type of the environment variable \"%v\". Type \"int\" was expected", envKey)
			os.Exit(1)
		}

		setField(&Config, key, parsedValue)
	} else {
		setField(&Config, key, value)
	}
}

func setField(obj interface{}, fieldName string, value interface{}) {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(fieldName)

	if !structFieldValue.IsValid() {
		log.Printf("Invalid field: %s", fieldName)
		return
	}

	if !structFieldValue.CanSet() {
		log.Printf("Cannot set field: %s", fieldName)
		return
	}

	fieldValue := reflect.ValueOf(value)
	if structFieldValue.Type() != fieldValue.Type() {
		log.Printf("Invalid type for field: %s", fieldName)
		return
	}

	structFieldValue.Set(fieldValue)
}
