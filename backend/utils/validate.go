package utils

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

var validate = validator.New()

func Validate(payload any) []string {
	var validationErrors []string
	if err := validate.Struct(payload); err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			validationErrors = append(
				validationErrors,
				fmt.Sprintf("%s validation failed on '%s' tag.", err.Field(), err.Tag()),
			)
		}
	}

	return validationErrors
}
