package adapters

import (
	"context"
	"tree/config"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

var _ctx context.Context
var _driver neo4j.DriverWithContext

func InitNeo4j() (neo4j.DriverWithContext, context.Context) {
	driver, err := neo4j.NewDriverWithContext(
		config.Config.Neo4jUrl,
		neo4j.BasicAuth(config.Config.Neo4jUser, config.Config.Neo4jPassword, ""),
	)
	if err != nil {
		panic(err)
	}

	_ctx = context.Background()
	_driver = driver

	return _driver, _ctx
}

func ExecQuery(query string, payload map[string]any) (*neo4j.EagerResult, error) {
	result, err := neo4j.ExecuteQuery(_ctx, _driver, query, payload, neo4j.EagerResultTransformer)
	return result, err
}
