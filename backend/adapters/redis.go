package adapters

import (
	"context"
	"errors"
	"fmt"
	"tree/config"

	"github.com/redis/go-redis/v9"
)

var cache *redis.Client
var redis_ctx context.Context

func InitCache() {
	redis_ctx = context.Background()
	cache = redis.NewClient(&redis.Options{
		Addr:     config.Config.RedisUrl,
		Password: config.Config.RedisPassword,
		DB:       0,
	})
}

func SetKey(key string, value string) {
	err := cache.Set(redis_ctx, key, value, 0).Err()
	if err != nil { 
		panic(err)
	}
}

func GetKey(key string) (string, error) {
	val, err := cache.Get(redis_ctx, key).Result()
	if err == redis.Nil {
		return "", errors.New(fmt.Sprintf("Key \" %v \" does not exist", key))
	} else if err != nil {
		panic(err)
	}

	return val, err
}

func DeleteKey(key string) error {
	_, err := cache.Del(redis_ctx, key).Result()
	if err == redis.Nil {
		return errors.New(fmt.Sprintf("Key \" %v \" does not exist", key))
	} else if err != nil {
		panic(err)
	}

	return nil
}
