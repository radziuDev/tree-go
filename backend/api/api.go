package api

import (
	"fmt"
	"log"
	"tree/api/branch"
	"tree/api/leaf"
	"tree/api/tree"
	"tree/api/ui"
	"tree/config"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func InitAPI() {
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	api := app.Group(fmt.Sprintf("/%v", config.Config.Prefix))
	ui.Init(api)
	tree.Init(api)
	branch.Init(api)
	leaf.Init(api)

	log.Fatalln(app.Listen(fmt.Sprintf(":%v", config.Config.Port)))
	log.Printf("API runnig at port %v", config.Config.Port)
}
