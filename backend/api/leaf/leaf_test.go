package leaf

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"tree/adapters"
	"tree/api/branch"
	"tree/config"
)

func TestMain(m *testing.M) {
	config.LoadConfigs()

	driver, ctx := adapters.InitNeo4j()
	defer driver.Close(ctx)
	adapters.InitCache()

	exitCode := m.Run()
	os.Exit(exitCode)
}

func TestCreateLeafSuccess(t *testing.T) {
	_, err := branch.Create(mockBranch["id"].(string), mockBranch["value"].(int))
	if err != nil {
		t.Fatal(err)
	}

	response := execAddLeafRequest(map[string]any{"rootedIn": mockBranch["id"]}, t)
	if response.StatusCode != http.StatusCreated {
		t.Errorf("Expected status code %d, got %d", http.StatusCreated, response.StatusCode)
		_, err = branch.Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		return
	}

	_, err = branch.Delete(mockBranch["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

func CreateLeafBadRequest(t *testing.T) {
	response := execAddLeafRequest(map[string]any{"rootedIn": 123}, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execAddLeafRequest(map[string]any{"rootedIn": nil}, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}
}

func DeleteLeafSuccess(t *testing.T) {
	_, err := Create(mockLeaf["id"].(string))
	if err != nil {
		t.Fatal(err)
	}

	response, err := execDeleteLeafRequest(mockLeaf["id"].(string), t)
	if err != nil {
		_, err = Delete(mockLeaf["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		return
	}

	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}
}

func DeleteLeafNotFound(t *testing.T) {
	_, err := Create(mockLeaf["id"].(string))
	if err != nil {
		t.Fatal(err)
	}

	response, err := execDeleteLeafRequest("_notFound", t)
	if err != nil {
		_, err = Delete(mockLeaf["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		return
	}

	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	_, err = Delete(mockLeaf["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

var mockBranch = map[string]any{"id": "lb0", "value": 420}
var mockLeaf = map[string]any{"id": "l00"}

func execAddLeafRequest(reqBody map[string]any, t *testing.T) *http.Response {
	payload, _ := json.Marshal(reqBody)

	response, err := http.Post(getEndpoint(""), "application/json", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatalf("Failed to send POST request: %v", err)
	}
	defer response.Body.Close()

	return response
}

func execDeleteLeafRequest(id string, t *testing.T) (*http.Response, error) {
	req, err := http.NewRequest("DELETE", getEndpoint("/"+id), nil)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}
	defer response.Body.Close()

	return response, err
}

func getEndpoint(value string) string {
	return fmt.Sprintf(
		"http://backend:%v/%v/leaf%v",
		config.Config.Port,
		config.Config.Prefix,
		value,
	)
}
