package leaf

import (
	"errors"

	"github.com/google/uuid"
)

func createLeaf(rootedIn string) error {
	isBranchExist, err := checkIfBranchExist(rootedIn)
	if err != nil {
		return err
	}
	if !isBranchExist {
		return errors.New(RootOfLeafNotExist)
	}

	leafId := uuid.New().String()
	_, err = Create(leafId)
	if err != nil {
		return err
	}

	err = RootInParent(leafId, rootedIn)
	return err
}

func checkIfBranchExist(id string) (bool, error) {
	results, err := GetById(id)
	if err != nil {
		return false, err
	}

	if len(results.Records) == 0 {
		return false, errors.New(BranchNotExist)
	}

	return true, err
}

func deleteLeaf(id string) error {
	results, err := Delete(id)
	if len(results.Records) == 0 {
		return errors.New(LeafNotExist)
	}

	return err
}
