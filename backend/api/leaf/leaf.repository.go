package leaf

import (
	"tree/adapters"
	"tree/api/tree"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
)

func Create(id string) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		"CREATE (l:Leaf {id: $id})",
		map[string]any{"id": id},
	)
}

func GetById(id string) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		"MATCH (b:Branch {id: $id}) RETURN b",
		map[string]any{"id": id})
}

func RootInParent(from string, to string) error {
	_, err := adapters.ExecQuery(
		`MATCH (l:Leaf {id: $from})
		 MATCH (b:Branch {id: $to})
		 CREATE (l)-[:ROOTED_IN]->(b)`,
		map[string]interface{}{
			"from": from,
			"to":   to,
		})

	tree, _ := tree.GetTreeByLeafId(from)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	return err
}

func Delete(id string) (*neo4j.EagerResult, error) {
	tree, _ := tree.GetTreeByLeafId(id)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	return adapters.ExecQuery(
		`MATCH (l:Leaf {id: $id})
		 DETACH DELETE l
		 RETURN l`,
		map[string]interface{}{"id": id})
}
