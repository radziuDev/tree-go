package leaf

const (
	UnexpectedError    string = "An unexpected error occurred."
	BranchNotExist            = "The branch does not exist."
	LeafNotExist              = "The leaf does not exist."
	RootOfLeafNotExist        = "The root of the leaf does not exist."
	BadRequest                = "Bad request."
)
