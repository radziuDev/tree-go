package leaf

import (
	"log"
	"tree/utils"

	"github.com/gofiber/fiber/v2"
)

func Init(api fiber.Router) {
	leafController := api.Group("/leaf")

	leafController.Post("",func(c *fiber.Ctx) error {
		var payload CreateLeafDTO
		if err := c.BodyParser(&payload); err != nil {
			return c.Status(fiber.StatusBadRequest).SendString(BadRequest)
		}

		validationErrors := utils.Validate(payload)
		if len(validationErrors) > 0 {
			return c.Status(fiber.StatusBadRequest).JSON(validationErrors)
		
		}
		err := createLeaf(payload.RootedIn)
		if err != nil {
			if err.Error() == BranchNotExist || err.Error() == RootOfLeafNotExist {
				return c.Status(fiber.StatusConflict).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusCreated).SendString("Successfully created.")
	})

	leafController.Delete(":id",func(c *fiber.Ctx) error {
		leafId := c.Params("id")
		err := deleteLeaf(leafId)
		if err != nil {
			if err.Error() == LeafNotExist {
				return c.Status(fiber.StatusNotFound).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}
		return c.Status(fiber.StatusOK).SendString("Successfully deleted.")
	})
}
