package leaf

type CreateLeafDTO struct {
	RootedIn string `json:"rootedIn" validate:"required"`
}
