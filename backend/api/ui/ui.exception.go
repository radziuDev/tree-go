package ui

const (
	TreeNotExist    string = "The tree does not exist."
	UnexpectedError        = "An unexpected error occurred."
	BadRequest             = "Bad request."
)
