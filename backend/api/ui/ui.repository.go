package ui

import (
	"tree/adapters"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

func getTrees() (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		"MATCH (t:Tree) RETURN t",
		map[string]interface{}{},
	)
}

func getTreeById(id string) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		`MATCH (t:Tree {id: $id}) return t`,
		map[string]interface{}{"id": id},
	)
}

func getTreeStrucureById(id string) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		`MATCH path = (t:Tree)<-[:ROOTED_IN*]-(child)
		 WHERE t.id = $id
		 RETURN path`,
		map[string]interface{}{"id": id},
	)
}
