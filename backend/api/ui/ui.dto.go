package ui

type Tree struct {
	Id   string `json:"id"`
	Name string `json:"name"`
}

type TreeNode struct {
	ParsedNode
	Childs []TreeNode
}

type ParsedNode struct {
	Id       int                    `json:"Id"`
	Type     string                 `json:"Type"`
	Props    map[string]interface{} `json:"Props"`
	RootedIn int                    `json:"RootedIn"`
}

type Node struct {
	Id       int                    `json:"Id"`
	Labels   []string               `json:"Labels"`
	Props    map[string]interface{} `json:"Props"`
	RootedIn int                    `json:"RootedIn"`
}

type Relationship struct {
	Id          int                    `json:"Id"`
	ElementId   string                 `json:"ElementId"`
	StartId     int                    `json:"StartId"`
	StartElemId string                 `json:"StartElementId"`
	EndId       *int                   `json:"EndId"`
	EndElemId   string                 `json:"EndElementId"`
	Type        string                 `json:"Type"`
	Props       map[string]interface{} `json:"Props"`
}

type Element struct {
	Values []struct {
		Nodes         []Node         `json:"Nodes"`
		Relationships []Relationship `json:"Relationships"`
	} `json:"Values"`
	Keys []string `json:"Keys"`
}
