package ui

import (
	"encoding/json"
	"fmt"
	"log"
)

func parseData(jsonData string) ([]ParsedNode, error) {
	var parsedData []ParsedNode
	var elements []Element
	if err := json.Unmarshal([]byte(jsonData), &elements); err != nil {
		return parsedData, err
	}

	for _, element := range elements {
		relationships := element.Values[0].Relationships
		nodes := element.Values[0].Nodes

		for _, node := range nodes {
			var relatedRel Relationship
			for _, rel := range relationships {
				if rel.StartId == node.Id {
					relatedRel = rel
					break
				}
			}

			rootedIn := -1
			if relatedRel.EndId != nil {
				rootedIn = *relatedRel.EndId
			}

			tmp := Node{
				Id:       node.Id,
				Labels:   node.Labels,
				Props:    node.Props,
				RootedIn: rootedIn,
			}

			parsedData = append(parsedData, ParsedNode{
				Id:       tmp.Id,
				Type:     tmp.Labels[0],
				Props:    tmp.Props,
				RootedIn: tmp.RootedIn,
			})
		}
	}

	var unique []ParsedNode
	nodeMap := make(map[string]ParsedNode)
	for _, obj := range parsedData {
		if _, exists := nodeMap[fmt.Sprintf("%v", obj)]; !exists {
			unique = append(unique, obj)
			nodeMap[fmt.Sprintf("%v", obj)] = obj
		}
	}

	return unique, nil
}

func createTree(structure []ParsedNode) TreeNode {
	nodeMap := make(map[int]ParsedNode)
	for _, node := range structure {
		nodeMap[node.Id] = node
	}

	var buildTree func(int, float64) TreeNode
	buildTree = func(nodeId int, value float64) TreeNode {
		node := nodeMap[nodeId]
		if node.Type == "Branch" {
			value = value + node.Props["value"].(float64)
		} else if node.Type == "Leaf" {
			node.Props["value"] = value
			log.Println(value)
		}

		childNodes := []TreeNode{}
		for _, childNode := range structure {
			if childNode.RootedIn == nodeId {
				childNodes = append(childNodes, buildTree(childNode.Id, value))
			}
		}
		return TreeNode{
			ParsedNode: node,
			Childs:     childNodes,
		}
	}

	root := ParsedNode{}
	for _, node := range structure {
		if node.Type == "Tree" {
			root = node
			break
		}
	}

	return buildTree(root.Id, 0)
}
