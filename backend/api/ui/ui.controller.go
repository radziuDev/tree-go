package ui

import (
	"encoding/json"
	"log"
	"tree/adapters"

	"github.com/gofiber/fiber/v2"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
)

func Init(api fiber.Router) {
	api.Get("tree", func(c *fiber.Ctx) error {
		var trees []Tree = []Tree{}
		cache, err := adapters.GetKey("trees")
		if err == nil {
			if err := json.Unmarshal([]byte(cache), &trees); err != nil {
				log.Println("Error:", err)
				return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
			}
			return c.Status(fiber.StatusOK).JSON(trees)
		}

		result, err := getTrees()
		if err != nil {
			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		for _, record := range result.Records {
			tree := Tree{
				Id:   record.Values[0].(dbtype.Node).GetProperties()["id"].(string),
				Name: record.Values[0].(dbtype.Node).GetProperties()["name"].(string),
			}
			trees = append(trees, tree)
		}

		jsonData, err := json.Marshal(trees)
		if err != nil {
			log.Println("Error:", err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		adapters.SetKey("trees", string(jsonData))
		return c.Status(fiber.StatusOK).JSON(trees)
	})

	api.Get("tree/:id", func(c *fiber.Ctx) error {
		treeId := c.Params("id")
		cache, err := adapters.GetKey(treeId)
		if err == nil {
			var cacheResponse TreeNode
			if err := json.Unmarshal([]byte(cache), &cacheResponse); err != nil {
				log.Println("Error:", err)
				return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
			}
			return c.Status(fiber.StatusOK).JSON(cacheResponse)
		}

		tree, err := getTreeById(treeId)
		if err != nil {
			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		result, err := getTreeStrucureById(treeId)
		if err != nil {
			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		if len(result.Records) == 0 && len(tree.Records) > 0 {
			return c.Status(fiber.StatusOK).JSON(tree.Records[0])
		} else if len(result.Records) == 0 {
			return c.Status(fiber.StatusNotFound).SendString(TreeNotExist)
		}

		jsonData, err := json.Marshal(result.Records)
		if err != nil {
			log.Println("Error:", err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		parsedData, err := parseData(string(jsonData))
		if err != nil {
			log.Println("Error:", err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		res := createTree(parsedData)
		resData, err := json.Marshal(res)
		if err != nil {
			log.Println("Error:", err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		adapters.SetKey(treeId, string(resData))
		return c.Status(fiber.StatusOK).JSON(res)
	})
}
