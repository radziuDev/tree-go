package ui

import (
	"fmt"
	"net/http"
	"os"
	"testing"
	"tree/adapters"
	"tree/config"
)

func TestMain(m *testing.M) {
	config.LoadConfigs()

	driver, ctx := adapters.InitNeo4j()
	defer driver.Close(ctx)
	adapters.InitCache()

	exitCode := m.Run()
	os.Exit(exitCode)
}

func TestGettingTreesSuccess(t *testing.T) {
	response, err := http.Get(getEndpoint(""))
	if err != nil {
		t.Fatalf("Failed to send GET request: %v", err)
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}
}

func TestGettingSpecificTreeSucces(t *testing.T) {
	_, err := adapters.ExecQuery(
		"CREATE (t:Tree {id: $id, name: $name})",
		map[string]any{"id": "QUE", "name": "test123"})
	if err != nil {
		t.Error(err)
	}

	response, err := http.Get(getEndpoint("/QUE"))
	if err != nil {
		t.Fatalf("Failed to send GET request: %v", err)
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}

	_, err = adapters.ExecQuery(
		"MATCH(t:Tree {id: $id}) DELETE t",
		map[string]any{"id": "QUE"},
	)

	if err != nil {
		t.Error(err)
	}
}

func TestGettingSpecificNotFound(t *testing.T) {
	response, err := http.Get(getEndpoint("/010"))
	if err != nil {
		t.Fatalf("Failed to send GET request: %v", err)
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, response.StatusCode)
	}
}

func getEndpoint(value string) string {
	return fmt.Sprintf("http://backend:%v/api/tree%v", config.Config.Port, value)
}
