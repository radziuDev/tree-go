package tree

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"tree/adapters"
	"tree/config"
)

func TestMain(m *testing.M) {
	config.LoadConfigs()

	driver, ctx := adapters.InitNeo4j()
	defer driver.Close(ctx)
	adapters.InitCache()

	exitCode := m.Run()
	os.Exit(exitCode)
}

func TestCreateTreeSuccess(t *testing.T) {
	response := execCreateRequest(mockTree["name"], t)
	if response.StatusCode != http.StatusCreated {
		t.Errorf("Expected status code %d, got %d", http.StatusCreated, response.StatusCode)
	}

	_, err := DeleteByName(mockTree["name"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

func TestCreateTreeConflict(t *testing.T) {
	_, err := Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Fatal(err)
	}

	response := execCreateRequest(mockTree["name"], t)
	if response.StatusCode != http.StatusConflict {
		t.Errorf("Expected status code %d, got %d", http.StatusConflict, response.StatusCode)
	}

	_, err = Delete(mockTree["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

func TestCreateTreeBadRequest(t *testing.T) {
	response := execCreateRequest("12", t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execCreateRequest(123, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execCreateRequest(nil, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execCreateRequest("01234567890123456789123", t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}
}

func TestEditTreeSucces(t *testing.T) {
	_, err := Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Error(err)
	}

	response, _ := execPatchTreeRequest(mockTree["id"].(string), []byte(`{"name": "test"}`), t)
	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}

	_, err = Delete(mockTree["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

func TestEditTreeNotFound(t *testing.T) {
	response, _ := execPatchTreeRequest("_NotFound", []byte(`{"name": "_test"}`), t)
	if response.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, response.StatusCode)
	}
}

func TestDeleteTreeSuccess(t *testing.T) {
	_, err := Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Error(err)
	}
	response, err := execDeleteTreeRequest("t00", t)
	if err != nil {
		t.Fatalf("Failed to send PATCH request: %v", err)
		_, err = Delete(mockTree["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		return
	}

	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
		_, err = Delete(mockTree["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
	}
}

func TestDeleteTreeNotFound(t *testing.T) {
	response, err := execDeleteTreeRequest("_NotFound", t)
	if err != nil {
		t.Fatalf("Failed to send PATCH request: %v", err)
	}

	if response.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, response.StatusCode)
	}
}

var mockTree = map[string]any{"id": "t00", "name": "Test123"}

func execCreateRequest(name any, t *testing.T) *http.Response {
	payload, _ := json.Marshal(map[string]any{"name": name})
	response, err := http.Post(getEndpoint(""), "application/json", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatalf("Failed to send POST request: %v", err)
	}

	defer response.Body.Close()
	return response
}

func execPatchTreeRequest(id string, body []byte, t *testing.T) (*http.Response, error) {
	req, err := http.NewRequest("PATCH", getEndpoint("/"+id+"/name"), bytes.NewBuffer(body))
	if err != nil {
		t.Fatalf("Failed to send PATCH request: %v", err)
		return nil, err
	}

	client := &http.Client{}
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send PATCH request: %v", err)
		return nil, err
	}

	defer response.Body.Close()
	return response, err
}

func execDeleteTreeRequest(id string, t *testing.T) (*http.Response, error) {
	req, err := http.NewRequest("DELETE", getEndpoint("/"+id), nil)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}

	defer response.Body.Close()
	return response, err
}

func getEndpoint(value string) string {
	return fmt.Sprintf(
		"http://backend:%v/%v/tree%v",
		config.Config.Port,
		config.Config.Prefix,
		value,
	)
}
