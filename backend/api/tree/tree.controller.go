package tree

import (
	"log"
	"tree/utils"

	"github.com/gofiber/fiber/v2"
)

func Init(api fiber.Router) {
	treeController := api.Group("/tree")

	treeController.Post("", func(c *fiber.Ctx) error {
		var payload CreateTreeDTO
		if err := c.BodyParser(&payload); err != nil {
			return c.Status(fiber.StatusBadRequest).SendString(BadRequest)
		}

		validationErrors := utils.Validate(payload)
		if len(validationErrors) > 0 {
			return c.Status(fiber.StatusBadRequest).JSON(validationErrors)
		}

		if err := createTree(payload.Name); err != nil {
			if err.Error() == TreeExist {
				return c.Status(fiber.StatusConflict).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusCreated).SendString("Successfully created.")
	})

	treeController.Patch(":id/name", func(c *fiber.Ctx) error {
		treeId := c.Params("id")
		var payload UpdateTreeDTO
		
		if err := c.BodyParser(&payload); err != nil {
			return c.Status(fiber.StatusBadRequest).SendString(BadRequest)
		}

		validationErrors := utils.Validate(payload)
		if len(validationErrors) > 0 {
			return c.Status(fiber.StatusBadRequest).JSON(validationErrors)
		}

		err := updateTreeName(treeId, payload.Name)
		if err != nil {
			if err.Error() == TreeExist {
				return c.Status(fiber.StatusConflict).SendString(err.Error())
			}
			if err.Error() == TreeNotExist {
				return c.Status(fiber.StatusNotFound).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusOK).SendString("Successfully updated.")
	})

	treeController.Delete(":id", func(c *fiber.Ctx) error {
		treeId := c.Params("id")
		err := deleteTree(treeId)
		if err != nil {
			if err.Error() == TreeNotExist {
				return c.Status(fiber.StatusNotFound).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusOK).SendString("Successfully deleted.")
	})
}
