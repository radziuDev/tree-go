package tree

type CreateTreeDTO struct {
	Name string `json:"name" validate:"required,min=3,max=20"`
}

type UpdateTreeDTO struct {
	Name string `json:"name" validate:"required,min=3,max=20"`
}
