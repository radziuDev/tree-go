package tree

import (
	"errors"
	"tree/adapters"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
)

func GetById(id string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"MATCH (t:Tree {id: $id}) RETURN t",
		map[string]interface{}{"id": id})
	return result, err
}

func GetByName(name string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"MATCH (t:Tree {name: $name}) RETURN t",
		map[string]interface{}{"name": name})
	return result, err
}

func GetTreeByBranchId(id string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"MATCH (n:Branch {id: $id})-[:ROOTED_IN*]->(t:Tree) RETURN t;",
		map[string]interface{}{"id": id})
	return result, err
}

func GetTreeByLeafId(id string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"MATCH (n:Leaf {id: $id})-[:ROOTED_IN*]->(t:Tree) RETURN t;",
		map[string]interface{}{"id": id})
	return result, err
}

func Create(id string, name string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"CREATE (t:Tree {id: $id, name: $name}) RETURN t",
		map[string]any{"id": id, "name": name})

	adapters.DeleteKey("trees")
	return result, err
}

func Update(id string, name string) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		"MATCH (t:Tree {id: $id}) SET t.name = $name RETURN t",
		map[string]interface{}{
			"id":   id,
			"name": name,
		})

	adapters.DeleteKey("trees")
	return result, err
}

func Delete(id string) (*neo4j.EagerResult, error) {
	adapters.DeleteKey("trees")
	result, err := adapters.ExecQuery(
		"MATCH (t:Tree {id: $id})<-[*]-(r) DETACH DELETE t, r RETURN t",
		map[string]interface{}{"id": id})
	if err != nil {
		return result, err
	}

	if len(result.Records) == 0 {
		result, err := adapters.ExecQuery(
			"MATCH (t:Tree {id: $id}) DETACH DELETE t RETURN t",
			map[string]interface{}{"id": id})
		if err != nil {
			return result, err
		}

		if len(result.Records) == 0 {
			return result, errors.New(TreeNotExist)
		}
	}
	return result, err
}

func DeleteByName(name string) (*neo4j.EagerResult, error) {
	adapters.DeleteKey("trees")
	result, err := adapters.ExecQuery(
		"MATCH (t:Tree {name: $name})<-[*]-(r) DETACH DELETE t, r RETURN t",
		map[string]interface{}{"name": name})
	if err != nil {
		return result, err
	}

	if len(result.Records) == 0 {
		result, err := adapters.ExecQuery(
			"MATCH (t:Tree {name: $name}) DETACH DELETE t RETURN t",
			map[string]interface{}{"name": name})
		if err != nil {
			return result, err
		}

		if len(result.Records) == 0 {
			return result, errors.New(TreeNotExist)
		}
	}
	return result, err
}
