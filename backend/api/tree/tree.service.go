package tree

import (
	"errors"

	"github.com/google/uuid"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
)

func createTree(name string) error {
	if _, err := getTreeByName(name); err == nil {
		return errors.New(TreeExist)
	}

	_, err := Create(uuid.New().String(), name)
	return err
}

func getTreeByName(name string) (Tree, error) {
	var tree Tree = Tree{}
	result, err := GetByName(name)
	if err != nil {
		return tree, err
	}

	if len(result.Records) == 0 {
		return tree, errors.New(TreeNotExist)
	}

	treeRecord := result.Records[0].Values[0].(dbtype.Node)
	tree = Tree{
		id:   treeRecord.GetProperties()["id"].(string),
		name: treeRecord.GetProperties()["name"].(string),
	}

	return tree, err
}

func updateTreeName(id string, name string) error {
	if _, err := getTreeByName(name); err == nil {
		return errors.New(TreeExist)
	}

	result, err := Update(id, name)
	if err != nil {
		return err
	}

	if len(result.Records) == 0 {
		return errors.New(TreeNotExist)
	}

	return err
}

func deleteTree(id string) error {
	_, err := Delete(id)
	return err
}
