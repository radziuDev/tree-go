package tree

const (
	TreeNotExist    string = "Tree not exist."
	TreeExist              = "A tree with this name exists."
	UnexpectedError        = "An unexpected error occurred."
	BadRequest             = "Bad request."
)
