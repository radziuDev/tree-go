package branch

const (
	BranchNotExist       string = "The branch does not exist."
	RootOfBranchNotExist        = "The root of the branch does not exist."
	UnexpectedError             = "An unexpected error occurred."
	BadRequest                  = "Bad request."
)
