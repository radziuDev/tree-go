package branch

import (
	"log"
	"tree/utils"

	"github.com/gofiber/fiber/v2"
)

func Init(api fiber.Router) {
	branchController := api.Group("/branch")

	branchController.Post("", func(c *fiber.Ctx) error {
		var payload CreateBrachDTO
		if err := c.BodyParser(&payload); err != nil {
			return c.Status(fiber.StatusBadRequest).SendString(BadRequest)
		}

		validationErrors := utils.Validate(payload)
		if len(validationErrors) > 0 {
			return c.Status(fiber.StatusBadRequest).JSON(validationErrors)
		}

		err := createBranch(payload.Value, payload.RootedIn, payload.IsMainBranch)
		if err != nil {
			if err.Error() == BranchNotExist || err.Error() == RootOfBranchNotExist {
				return c.Status(fiber.StatusConflict).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusCreated).SendString("Successfully created.")
	})

	branchController.Put(":id", func(c *fiber.Ctx) error {
		branchId := c.Params("id")
		var payload UpdateBranchDTO
		if err := c.BodyParser(&payload); err != nil {
			return c.Status(fiber.StatusBadRequest).SendString(BadRequest)
		}

		validationErrors := utils.Validate(payload)
		if len(validationErrors) > 0 {
			return c.Status(fiber.StatusBadRequest).JSON(validationErrors)
		}

		err := updateBranch(branchId, payload.Value, payload.RootedIn, payload.IsMainBranch)
		if err != nil {
			if err.Error() == BranchNotExist {
				return c.Status(fiber.StatusNotFound).SendString(err.Error())
			}
			if err.Error() == RootOfBranchNotExist {
				return c.Status(fiber.StatusConflict).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusOK).SendString("Successfully updated.")
	})

	branchController.Delete(":id", func(c *fiber.Ctx) error {
		branchId := c.Params("id")
		err := deleteBranch(branchId)
		if err != nil {
			if err.Error() == BranchNotExist || err.Error() == RootOfBranchNotExist {
				return c.Status(fiber.StatusNotFound).SendString(err.Error())
			}

			log.Println(err)
			return c.Status(fiber.StatusInternalServerError).SendString(UnexpectedError)
		}

		return c.Status(fiber.StatusOK).SendString("Successfully deleted.")
	})
}
