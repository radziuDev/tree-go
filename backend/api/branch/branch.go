package branch

type Branch struct {
	id    string
	value int64
}
