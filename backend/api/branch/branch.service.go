package branch

import (
	"errors"
	"tree/api/tree"

	"github.com/google/uuid"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
)

func createBranch(value int, rootedIn string, isMainBranch bool) error {
	if isMainBranch {
		isTreeExist, err := checkIfTreeExist(rootedIn)
		if err != nil {
			return err
		}
		if !isTreeExist {
			return errors.New(RootOfBranchNotExist)
		}
	} else {
		_, err := getBranchById(rootedIn)
		if err != nil {
			return errors.New(RootOfBranchNotExist)
		}
	}

	branchId := uuid.New().String()
	_, err := Create(branchId, value)
	if err != nil {
		return err
	}

	err = RootInParent(branchId, rootedIn, isMainBranch)
	return err
}

func getBranchById(id string) (Branch, error) {
	branch := Branch{}

	results, err := GetById(id)
	if err != nil {
		return branch, err
	}

	if len(results.Records) == 0 {
		return branch, errors.New(BranchNotExist)
	}

	branchRecord := results.Records[0].Values[0].(dbtype.Node)
	branch = Branch{
		id:    branchRecord.GetProperties()["id"].(string),
		value: branchRecord.GetProperties()["value"].(int64),
	}

	return branch, err
}

func checkIfTreeExist(id string) (bool, error) {
	result, err := tree.GetById(id)
	if err != nil {
		return false, err
	}

	if len(result.Records) == 0 {
		return false, err
	}

	return true, err
}

func updateBranch(branchId string, value int, rootedIn string, isMainBranch bool) error {
	if _, err := getBranchById(branchId); err != nil {
		return errors.New(BranchNotExist)
	}

	if isMainBranch {
		isExist, err := checkIfTreeExist(rootedIn)
		if err != nil {
			return err
		}
		if !isExist {
			return errors.New(RootOfBranchNotExist)
		}
	} else {
		if _, err := getBranchById(rootedIn); err != nil {
			return errors.New(RootOfBranchNotExist)
		}
	}

	_, err := Update(branchId, value)
	if err != nil {
		return err
	}

	_, err = DeleteRootConnection(branchId)
	if err != nil {
		return err
	}

	err = RootInParent(branchId, rootedIn, isMainBranch)
	return err
}

func deleteBranch(branchId string) error {
	if _, err := getBranchById(branchId); err != nil {
		return errors.New(RootOfBranchNotExist)
	}

	results, err := Delete(branchId)
	if len(results.Records) == 0 {
		return errors.New(BranchNotExist)
	}

	return err
}
