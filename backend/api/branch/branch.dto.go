package branch

type CreateBrachDTO struct {
	Value        int    `json:"value" validate:"required"`
	RootedIn     string `json:"rootedIn" validate:"required"`
	IsMainBranch bool   `json:"isMainBranch"`
}

type UpdateBranchDTO struct {
	Value        int    `json:"value" validate:"required"`
	RootedIn     string `json:"rootedIn" validate:"required"`
	IsMainBranch bool   `json:"isMainBranch"`
}
