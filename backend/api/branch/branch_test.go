package branch

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"testing"
	"tree/adapters"
	"tree/api/tree"
	"tree/config"
)

func TestMain(m *testing.M) {
	config.LoadConfigs()

	driver, ctx := adapters.InitNeo4j()
	defer driver.Close(ctx)
	adapters.InitCache()

	exitCode := m.Run()
	os.Exit(exitCode)
}

func TestCreateBranchSuccess(t *testing.T) {
	_, err := tree.Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Fatal(err)
	}

	response := execAddBranchRequest(map[string]any{
		"rootedIn":     mockTree["id"].(string),
		"value":        mockBranch["value"].(int),
		"isMainBranch": mockBranch["isMainBranch"].(bool),
	}, t)
	if response.StatusCode != http.StatusCreated {
		t.Errorf("Expected status code %d, got %d", http.StatusCreated, response.StatusCode)
		_, err = tree.Delete(mockTree["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		return
	}

	_, err = tree.Delete(mockTree["id"].(string))
	if err != nil {
		t.Error(err)
	}
}

func TestCreateBranchBadRequest(t *testing.T) {
	_, err := Create(mockBranch["id"].(string), mockBranch["value"].(int))
	if err != nil {
		t.Fatal(err)
	}

	response := execAddBranchRequest(map[string]any{
		"rootedIn":     "_notFound",
		"value":        mockBranch["value"].(int),
		"isMainBranch": mockBranch["isMainBranch"].(bool),
	}, t)
	if response.StatusCode != http.StatusConflict {
		t.Errorf("Expected status code %d, got %d", http.StatusConflict, response.StatusCode)
	}

	response = execAddBranchRequest(map[string]any{
		"rootedIn":     mockBranch["id"].(string),
		"value":        mockBranch["value"].(int),
		"isMainBranch": true,
	}, t)
	if response.StatusCode != http.StatusConflict {
		t.Errorf("Expected status code %d, got %d", http.StatusConflict, response.StatusCode)
	}

	response = execAddBranchRequest(map[string]any{
		"rootedIn":     mockBranch["id"].(string),
		"value":        "_string",
		"isMainBranch": mockBranch["isMainBranch"].(bool),
	}, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execAddBranchRequest(map[string]any{
		"rootedIn":     mockBranch["id"].(string),
		"value":        nil,
		"isMainBranch": mockBranch["isMainBranch"].(bool),
	}, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	response = execAddBranchRequest(map[string]any{
		"rootedIn":     mockBranch["id"].(string),
		"value":        mockBranch["value"].(int),
		"isMainBranch": "_string",
	}, t)
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	_, err = Delete(mockBranch["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
}

func TestUpdateBranchSuccess(t *testing.T) {
	_, err := tree.Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Fatal(err)
	}
	_, err = Create(mockBranch["id"].(string), mockBranch["value"].(int))
	if err != nil {
		t.Fatal(err)
	}

	response, err := execPutBranchRequest(
		"b00",
		[]byte(`{"value": 23, "rootedIn": "tb0", "isMainBranch": true}`),
		t,
	)
	if err != nil {
		_, err = tree.Delete(mockTree["id"].(string))
		if err != nil {
			t.Error(err)
		}
		_, err = Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Fatalf("Failed to send PUT request: %v", err)
		return
	}
	if response.StatusCode != http.StatusOK {
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}

	_, err = adapters.ExecQuery(
		"MATCH (t:Tree {id: $from})<-[r:ROOTED_IN]-(b:Branch) DELETE t, b, r",
		map[string]any{"from": "tb0"},
	)
	if err != nil {
		t.Error(err)
	}
}

func TestUpdateBranchBadRequest(t *testing.T) {
	_, err := tree.Create(mockTree["id"].(string), mockTree["name"].(string))
	if err != nil {
		t.Fatal(err)
	}
	_, err = Create(mockBranch["id"].(string), mockBranch["value"].(int))
	if err != nil {
		t.Fatal(err)
	}

	response, err := execPutBranchRequest(
		"000",
		[]byte(`{"value": 23, "rootedIn": "tb0", "isMainBranch": true}`),
		t,
	)
	if err != nil {
		_, err = Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Fatalf("Failed to send PUT request: %v", err)
		return
	}
	if response.StatusCode != http.StatusNotFound {
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, response.StatusCode)
	}

	response, err = execPutBranchRequest(
		"b00",
		[]byte(`{"value": 23, "rootedIn": "010", "isMainBranch": true}`),
		t,
	)
	if err != nil {
		_, err = Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Fatalf("Failed to send PUT request: %v", err)
		return
	}
	if response.StatusCode != http.StatusConflict {
		t.Errorf("Expected status code %d, got %d", http.StatusConflict, response.StatusCode)
	}

	response, err = execPutBranchRequest(
		"b00",
		[]byte(`{"value": "23", "rootedIn": "tb0", "isMainBranch": true}`),
		t,
	)
	if err != nil {
		_, err = Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Fatalf("Failed to send PUT request: %v", err)
		return
	}
	if response.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, response.StatusCode)
	}

	_, err = Delete(mockBranch["id"].(string))
	if err != nil {
		t.Fatal(err)
	}
	_, err = tree.Delete(mockTree["id"].(string))
	if err != nil {
		t.Error(err)
	}
}

func TestDeleteBranchSuccess(t *testing.T) {
	_, err := Create(mockBranch["id"].(string), mockBranch["value"].(int))
	if err != nil {
		t.Fatal(err)
	}

	response, err := execDeleteBranchRequest("b00", t)
	if err != nil {
		_, err := Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Fatalf("Failed to send DELETE request: %v", err)
		return
	}

	if response.StatusCode != http.StatusOK {
		_, err := Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Errorf("Expected status code %d, got %d", http.StatusOK, response.StatusCode)
	}
}

func TestDeleteBranchNotFound(t *testing.T) {
	response, err := execDeleteBranchRequest("000", t)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return
	}

	if response.StatusCode != http.StatusNotFound {
		_, err := Delete(mockBranch["id"].(string))
		if err != nil {
			t.Fatal(err)
		}
		t.Errorf("Expected status code %d, got %d", http.StatusNotFound, response.StatusCode)
	}
}

var mockTree = map[string]any{"id": "tb0", "name": "test"}
var mockBranch = map[string]any{"id": "b00", "value": 123, "isMainBranch": true}

func execAddBranchRequest(reqBody map[string]any, t *testing.T) *http.Response {
	payload, _ := json.Marshal(reqBody)
	response, err := http.Post(getEndpoint(""), "application/json", bytes.NewBuffer(payload))
	if err != nil {
		t.Fatalf("Failed to send POST request: %v", err)
	}
	defer response.Body.Close()

	return response
}

func execPutBranchRequest(id string, body []byte, t *testing.T) (*http.Response, error) {
	req, err := http.NewRequest("PUT", getEndpoint("/"+id), bytes.NewBuffer(body))
	if err != nil {
		t.Fatalf("Failed to send PUT request: %v", err)
		return nil, err
	}

	client := &http.Client{}
	req.Header.Set("Content-Type", "application/json")
	response, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send PUT request: %v", err)
		return nil, err
	}

	defer response.Body.Close()
	return response, err
}

func execDeleteBranchRequest(id string, t *testing.T) (*http.Response, error) {
	req, err := http.NewRequest("DELETE", getEndpoint("/"+id), nil)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}

	client := &http.Client{}
	response, err := client.Do(req)
	if err != nil {
		t.Fatalf("Failed to send DELETE request: %v", err)
		return nil, err
	}

	defer response.Body.Close()
	return response, err
}

func getEndpoint(value string) string {
	return fmt.Sprintf(
		"http://backend:%v/%v/branch%v",
		config.Config.Port,
		config.Config.Prefix,
		value,
	)
}
