package branch

import (
	"tree/adapters"
	"tree/api/tree"

	"github.com/neo4j/neo4j-go-driver/v5/neo4j"
	"github.com/neo4j/neo4j-go-driver/v5/neo4j/dbtype"
)

func Create(id string, value int) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		"CREATE (b:Branch {id: $id, value: $value}) RETURN b",
		map[string]any{
			"id":    id,
			"value": value,
		})
}

func GetById(id string) (*neo4j.EagerResult, error) {
	return adapters.ExecQuery(
		"MATCH (b:Branch {id: $id}) RETURN b",
		map[string]any{"id": id})
}

func RootInParent(from string, to string, isMainBranch bool) error {
	query :=
		`MATCH (b1:Branch {id: $from})
		 MATCH (b2:Branch {id: $to})
		 CREATE (b1)-[:ROOTED_IN]->(b2)`

	if isMainBranch {
		query =
			`MATCH (b:Branch {id: $from})
			 MATCH (t:Tree {id: $to})
			 CREATE (b)-[:ROOTED_IN]->(t)`
	}

	_, err := adapters.ExecQuery(
		query,
		map[string]interface{}{"from": from, "to": to},
	)

	tree, _ := tree.GetTreeByBranchId(from)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	return err
}

func Update(id string, value int) (*neo4j.EagerResult, error) {
	result, err := adapters.ExecQuery(
		`MATCH (b:Branch {id: $id})
		 SET b.value = $value 
		 RETURN b`,
		map[string]interface{}{"id": id, "value": value},
	)

	tree, _ := tree.GetTreeByBranchId(id)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	return result, err
}

func DeleteRootConnection(id string) (*neo4j.EagerResult, error) {
	tree, _ := tree.GetTreeByBranchId(id)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	return adapters.ExecQuery(
		`MATCH (b1:Branch {id: $id})-[r:ROOTED_IN]->() DELETE r RETURN r`,
		map[string]interface{}{"id": id},
	)
}

func Delete(id string) (*neo4j.EagerResult, error) {
	tree, _ := tree.GetTreeByBranchId(id)
	if len(tree.Records) > 0 {
		treeRec := tree.Records[0].Values[0].(dbtype.Node)
		treeid := treeRec.GetProperties()["id"].(string)
		adapters.DeleteKey(treeid)
	}

	result, err := adapters.ExecQuery(
		`MATCH (b:Branch {id: $id})<-[*]-(r) DETACH DELETE b, r RETURN b`,
		map[string]interface{}{"id": id})

	if len(result.Records) == 0 {
		singleResult, err := adapters.ExecQuery(
			`MATCH (b:Branch {id: $id}) DETACH DELETE b RETURN b`,
			map[string]interface{}{"id": id})
		if err != nil {
			return singleResult, err
		}

		return singleResult, err
	}

	return result, err
}
