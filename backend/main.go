package main

import (
	"tree/api"
	"tree/config"
	"tree/adapters"
)

func main() {
	config.LoadConfigs()

	driver, ctx := adapters.InitNeo4j()
	defer driver.Close(ctx)

	adapters.InitCache()

	api.InitAPI()
}
