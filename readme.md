## Requirements
+ docker
+ docker-compose

## How to run it?
Run this in root folder. It also run test. Frontend is running at [http://127.0.0.1:3000/](http://127.0.0.1:3000/).
```sh
sh ./deploy.sh
```
To turn off type the command:
```sh
docker-compose down --volumes
```

## Architecture
![architecture graph](./images/architecture.png)

I decided to go with DDD and CQRS architecture. In the /api folder, the structure is divided into commands aggregated in folders and a UI folder responsible for queries.

## Endpoints
This is a typical REST API. By default, docker exposes it on port :5000

#### GET _/api/tree_
_Returns a list of trees_
#### GET _/api/tree/:id_
_Returns the constructed tree_
#### POST _/api/tree_
_Adds a tree_
```
	Name string `json:"name" validate:"required,min=3,max=20"`
```
#### PATCH _/api/tree/:id/name_
_Edits the tree_
```
	Name string `json:"name" validate:"required,min=3,max=20"`
```
#### DELETE _/api/tree/:id_
_Removes the tree_

#### POST _/api/branch_
_Adds a branch to the structure_
```
	Value        int    `json:"value" validate:"required"`
	RootedIn     string `json:"rootedIn" validate:"required"`
	IsMainBranch bool   `json:"isMainBranch"`
```
#### PUT _/api/branch/:id_
_Edits the branch_
```
	Value        int    `json:"value" validate:"required"`
	RootedIn     string `json:"rootedIn" validate:"required"`
	IsMainBranch bool   `json:"isMainBranch"`
```
#### DELETE _/api/branch/:id_
_Removes the branch_
#### POST /api/leaf
_Adds a leaf to the structure_
```
	RootedIn string `json:"rootedIn" validate:"required"`
```
#### DELETE _/api/leaf/:id_
_Removes the branch_


## Database
![database graph](./images/database.png)

I chose a graph database because the tree structure is a graph and Neo4j is optimized for finding connections as here parent -> child and will be more efficient than SQL databases.

## UI
![ui preview](./images/UI.png)

A simple app in nuxt3, nothing crazy.
